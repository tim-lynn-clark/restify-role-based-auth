// Node/NPM Modules
const expect    = require('chai').expect;
const path      = require('path');

// Local Modules
const app       = require('../lib/index');

// Local Variables
const aclFilePath = path.resolve('./tests/acl');

// Tests
describe('Role-Based Auth', () => {

    describe('exceptional cases', () => {
        it('should throw error when passed bad ACL folder path', () => {
            expect(() => app({
                aclPath: './doesNotExist'
            })).to.throw();
        });

        it('should throw error when passed bad ACL file name', () => {
            expect(() => app({
                aclFilename: 'doesNotExist.json'
            })).to.throw();
        });
    });

    describe('success cases', () => {
        it('should return a function when no errors are thrown', () => {
            expect(app({
                aclPath: aclFilePath,
                aclFilename: 'acl.json'
            })).to.be.a('function');
        });
    });
});