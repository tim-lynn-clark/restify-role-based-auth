// Node/NPM Modules
const expect     = require('chai').expect;
const restErrors = require('restify-errors');

// Local Modules
const authProcessor = require('../lib/processors/authorizationProcessor');

// Local Variables
const acl = [
    {
        "role": "admin",
        "permissions": [
            {
                "resource": "*",
                "methods": "*",
                "action": "allow"
            }
        ]
    },
    {
        "role": "user",
        "permissions": [
            {
                "resource": "user",
                "methods": [
                    "PUT",
                    "GET"
                ],
                "action": "allow"
            },
            {
                "resource": "userRestrictedRoute",
                "methods": [
                    "GET"
                ],
                "action": "deny"
            }
        ]
    },
    {
        "role": "restricted",
        "permissions": [
            {
                "resource": "*",
                "methods": [
                    "PUT"
                ],
                "action": "deny"
            }
        ]
    },
    {
        "role": "restricted2",
        "permissions": [
            {
                "resource": "admin",
                "methods": "*",
                "action": "deny"
            }
        ]
    },
    {
        "role": "superRestricted",
        "permissions": [
            {
                "resource": "*",
                "methods": "*",
                "action": "deny"
            }
        ]
    },
    {
        "role": "putAllowedEverywhere",
        "permissions": [
            {
                "resource": "*",
                "methods": [
                    "PUT"
                ],
                "action": "allow"
            }
        ]
    },
    {
        "role": "allMethodsAllowedOnUser",
        "permissions": [
            {
                "resource": "user",
                "methods": "*",
                "action": "allow"
            }
        ]
    }
];

const route = {
    name: 'putuser',
    method: 'PUT',
    path: '/user',
    spec: {
        method: 'PUT',
        path: '/user',
        version: '1.0.0'
    }
};

const config  = {
    aclFilename:   'acl.json',
    aclPath:       './defaults',
    baseApiUrl:    '/',
    errorMessage:  'You are not authorized to access this resource'
};

// Tests
describe('Authentication Processor', () => {

    describe('exceptional cases', () => {

        describe('return a 401 Unauthorized', () => {

            describe('parameter validations', () => {

                it('should  when ACL is null', (done) => {

                    let roles   = ['user'];
                    let acl     = null;

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should  when ACL is undefined', (done) => {

                    let roles   = ['user'];
                    let acl     = undefined;

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should  when ACL is empty', (done) => {

                    let roles   = ['user'];
                    let acl     = [];

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should when roles is null', (done) => {

                    let roles   = null;

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should  when roles is undefined', (done) => {

                    let roles   = undefined;

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should  when roles array is empty', (done) => {

                    let roles   = [];

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should  when route is null', (done) => {

                    let roles   = ['user'];
                    let route = null;

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should  when route is not an object', (done) => {

                    let roles   = ['user'];
                    let route = 1;

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });
            });

            describe('policy checks', () => {

                it('should  when the ACL does not contain a Policy for any of the Role(s) provided', (done) => {

                    let roles   = ['unknownUserType'];

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should  when a policy for a role is found but a permission object cannot be found for the resource requested', (done) => {

                    let roles   = ['user'];
                    let clone = JSON.parse(JSON.stringify(route));
                    clone.path = '/admin';

                    authProcessor(config, acl, roles, clone, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should  when a policy for a role is found, a permission object is found for the resource requested, the method being used is not in the policy', (done) => {

                    let roles   = ['user'];
                    let clone = JSON.parse(JSON.stringify(route));
                    clone.method = 'POST';

                    authProcessor(config, acl, roles, clone, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should when a policy for a role is found, a permissions object is found, the resource in the policy is the wildcard symbol (*), and there is at least one method, and the action is deny', (done) => {

                    let roles   = ['restricted'];

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should when a policy for a role is found, a permissions object is found for the resource requested, the method used in the policy is a string containing the wildcard symbol (*), and the action is deny', (done) => {

                    let roles   = ['restricted2'];

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should when a policy for a role is found, a permissions object is found, the resource in the policy is the wildcard symbol (*), the method used in the policy is a string containing the wildcard symbol (*), and the action is deny', (done) => {

                    let roles   = ['superRestricted'];

                    authProcessor(config, acl, roles, route, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });

                it('should  when a policy for a role is found, a permission object is found for the resource requested, the method being used is in the policy, and the action defined in the policy is deny', (done) => {

                    let roles   = ['user'];
                    let clone = JSON.parse(JSON.stringify(route));
                    clone.path = '/userRestrictedRoute';

                    authProcessor(config, acl, roles, clone, (error) => {

                        expect(error).to.be.an.instanceOf(restErrors.UnauthorizedError);
                        expect(error.message).to.eql(config.errorMessage);

                        done();
                    });
                });
            });
        });
    });

    describe('success cases', () => {

        describe('move to next middleware', () => {

            it('should when a policy for a role is found, a permissions object is found, the resource requested is the wildcard symbol (*), and the method being used is in the list, and the action is allow', (done) => {

                let roles   = ['putAllowedEverywhere'];

                authProcessor(config, acl, roles, route, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should when a policy for a role is found, a permissions object is found for the resource requested, the method used in the policy is a string containing the wildcard symbol (*), and the action is allow', (done) => {

                let roles   = ['allMethodsAllowedOnUser'];

                authProcessor(config, acl, roles, route, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should when a policy for a role is found, a permissions object is found, the resource requested is the wildcard symbol (*), the method used in the policy is a string containing the wildcard symbol (*), and the action is allow', (done) => {

                let roles   = ["admin"];

                authProcessor(config, acl, roles, route, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should when a policy for a role is found, a permission object is found for the resource requested, the method being used is in the policy, and the action defined in the policy is allow', (done) => {

                let roles   = ["user"];

                authProcessor(config, acl, roles, route, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle uppercase method in route and uppercase method in permission', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.method = 'PUT';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle lowercase method in route and lowercase method in permission', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.method = 'put';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "put"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle uppercase method in route and lowercase method in permission', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.method = 'PUT';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "put"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle lowercase method in route and uppercase method in permission', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.method = 'put';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle uppercase path in route and uppercase resource in permission', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.path = '/USER';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "USER",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle lowercase path in route and lowercase resource in permission', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.path = '/user';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle uppercase path in route and lowercase resource in permission', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.path = '/USER';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle lowercase path in route and uppercase resource in permission', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.path = '/user';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "USER",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle uppercase role in roles and uppercase role in policy', (done) => {

                let roles   = ["USER"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.path = '/user';
                let acl = [
                    {
                        "role": "USER",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle lowercase role in roles and lowercase role in policy', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.path = '/user';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle uppercase role in roles and lowercase role in policy', (done) => {

                let roles   = ["USER"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.path = '/user';
                let acl = [
                    {
                        "role": "user",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle lowercase role in roles and uppercase role in policy', (done) => {

                let roles   = ["user"];
                let cloneRoute = JSON.parse(JSON.stringify(route));
                cloneRoute.path = '/user';
                let acl = [
                    {
                        "role": "USER",
                        "permissions": [
                            {
                                "resource": "user",
                                "methods": [
                                    "PUT"
                                ],
                                "action": "allow"
                            }
                        ]
                    }
                ];

                authProcessor(config, acl, roles, cloneRoute, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle multiple methods no matter their order', (done) => {

                let roles   = ["user"];

                let acl = [{
                    "role": "user",
                    "permissions": [
                        {
                            "resource": "user",
                            "methods": [
                                "PUT",
                                "GET"
                            ],
                            "action": "allow"
                        }
                    ]
                }];

                authProcessor(config, acl, roles, route, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });

            it('should handle multiple methods no matter their order', (done) => {

                let roles   = ["user"];

                let acl = [{
                    "role": "user",
                    "permissions": [
                        {
                            "resource": "user",
                            "methods": [
                                "GET",
                                "PUT"
                            ],
                            "action": "allow"
                        }
                    ]
                }];

                authProcessor(config, acl, roles, route, (error) => {

                    expect(error).to.be.undefined;

                    done();
                });
            });
        });
    });
});