// Node/NPM Modules
const expect    = require('chai').expect;
const path      = require('path');

// Local Modules
const validator = require('../lib/validations/acl/aclValidator');

// Local Variables
const aclFilePath = path.resolve('./tests/acl/acl.json');
const badAclFilePath = path.resolve('./tests/acl/bad-acl.json');
const emptyAclFilePath = path.resolve('./tests/acl/empty-acl.json');
const emptyArrayAclFilePath = path.resolve('./tests/acl/empty-array-acl.json');
const nonObjectArrayAclFilePath = path.resolve('./tests/acl/non-object-array-acl.json');
const nonArrayAclFilePath = path.resolve('./tests/acl/non-array-acl.json');
const mixArrayAclFilePath = path.resolve('./tests/acl/mix-array-acl.json');

// Tests
describe('ACL File Validator', () => {

    describe('exceptional cases', () => {
        it('should throw error when passed a non-existent ACL file path', () => {
            expect(() => validator('bad-file-path')).to.throw();
        });

        it('should throw error when passed a bad ACL file', () => {
            expect(() => validator(badAclFilePath)).to.throw();
        });

        it('should throw error when ACL does not contain JSON', () => {
            expect(() => validator(emptyAclFilePath)).to.throw();
        });

        it('should throw error when ACL does not have at least one policy object', () => {
            expect(() => validator(emptyArrayAclFilePath)).to.throw();
        });

        it('should throw error when ACL does not have an array as its root', () => {
            expect(() => validator(nonArrayAclFilePath)).to.throw();
        });

        it('should throw error when ACL array indexes contain something other than objects', () => {
            expect(() => validator(nonObjectArrayAclFilePath)).to.throw();
        });

        it('should throw error when ACL array indexes contain a mix of objects and something else', () => {
            expect(() => validator(mixArrayAclFilePath)).to.throw();
        });
    });

    describe('success cases', () => {
        it('should return a valid array when passed a valid ACL file', () => {
            const aclData = validator(aclFilePath);
            expect(aclData).to.be.an('array');
        });

        it('should have at least one group policy object in a valid ACL file', () => {
            const aclData = validator(aclFilePath);
            expect(aclData).to.have.lengthOf.above(0);
        });
    });
});