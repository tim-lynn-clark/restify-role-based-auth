// Node/NPM Modules
const expect    = require('chai').expect;

// Local Modules
const validator = require('../lib/validations/auth/userValidator');

// Local Variables
const user = {
    roles: ['user']
};

// Tests
describe('User Validator', () => {

    it('should return an empty array when user is null', () => {
        let roles = validator(null);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user is undefined', () => {
        let roles = validator(undefined);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user.role property does not exist', () => {
        let clone = JSON.parse(JSON.stringify(user));
        delete clone.roles;
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user.role is null', () => {
        let clone = JSON.parse(JSON.stringify(user));
        delete clone.roles;
        clone.role = null;
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user.role is undefined', () => {
        let clone = JSON.parse(JSON.stringify(user));
        delete clone.roles;
        clone.role = undefined;
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user.role is not a String', () => {
        let clone = JSON.parse(JSON.stringify(user));
        delete clone.roles;
        clone.role = 1;
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user.roles property does not exist', () => {
        let clone = JSON.parse(JSON.stringify(user));
        delete clone.roles;
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user.roles is null', () => {
        let clone = JSON.parse(JSON.stringify(user));
        clone.roles = null;
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user.roles is undefined', () => {
        let clone = JSON.parse(JSON.stringify(user));
        clone.roles = undefined;
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user.roles is not an Array', () => {
        let clone = JSON.parse(JSON.stringify(user));
        clone.roles = 1;
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return an empty array when user.roles Array has no elements', () => {
        let clone = JSON.parse(JSON.stringify(user));
        clone.roles = [];
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(0);
    });

    it('should return a Roles Array with a single element when passed a Role of type String', () => {
        let clone = JSON.parse(JSON.stringify(user));
        clone.role = 'user';
        let roles = validator(clone);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf(1);
    });

    it('should return a Roles Array with one or more elements when passed a Role of type Array', () => {
        let roles = validator(user);
        expect(roles).to.be.an('array');
        expect(roles).to.have.lengthOf.above(0);
    });

});