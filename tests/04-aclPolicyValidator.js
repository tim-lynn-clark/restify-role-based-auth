// Node/NPM Modules
const expect    = require('chai').expect;

// Local Modules
const validator = require('../lib/validations/acl/aclPolicyValidator');

// Local Variables
const policy = {
    "role": "user",
    "permissions": [{
        "resource": "users",
        "methods": [
            "POST",
            "GET",
            "PUT"
        ],
        "action": "allow"
    }]
};

// Tests
describe('ACL Policy Validator', () => {

    describe('exceptional cases', () => {

        it('should throw error when policy is not a valid object literal', () => {
            expect(() => validator("Not an object literal")).to.throw();
        });

        it('should throw error when policy does not have a role property', () => {

            let clone = JSON.parse(JSON.stringify(policy));
            delete clone.role;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when policy role property is not a string', () => {

            let clone = JSON.parse(JSON.stringify(policy));
            clone.role = 1;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when policy does not have a permissions property', () => {

            let clone = JSON.parse(JSON.stringify(policy));
            delete clone.permissions;

            expect(() => validator(clone)).to.throw();
        });

        it('should throw error when permissions property is not an array', () => {

            let clone = JSON.parse(JSON.stringify(policy));
            clone.permissions = "not the right type";

            expect(() => validator(clone)).to.throw();
        });

    });

    describe('success cases', () => {
        it('should return a valid policy object when given a valid policy object', () => {
            const aclData = validator(policy);
            expect(aclData).to.be.an('object');
            expect(aclData).to.equal(policy);
        });
    });
});