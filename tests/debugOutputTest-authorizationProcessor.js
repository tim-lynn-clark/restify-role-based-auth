const authProcessor = require('../lib/processors/authorizationProcessor');

// Local Variables
const acl = [
    {
        "role": "admin",
        "permissions": [
            {
                "resource": "*",
                "methods": "*",
                "action": "allow"
            }
        ]
    },
    {
        "role": "user",
        "permissions": [
            {
                "resource": "user",
                "methods": [
                    "GET",
                    "PUT"
                ],
                "action": "allow"
            },
            {
                "resource": "userRestrictedRoute",
                "methods": [
                    "GET"
                ],
                "action": "deny"
            }
        ]
    },
    {
        "role": "restricted",
        "permissions": [
            {
                "resource": "*",
                "methods": [
                    "PUT"
                ],
                "action": "deny"
            }
        ]
    },
    {
        "role": "restricted2",
        "permissions": [
            {
                "resource": "admin",
                "methods": "*",
                "action": "deny"
            }
        ]
    },
    {
        "role": "superRestricted",
        "permissions": [
            {
                "resource": "*",
                "methods": "*",
                "action": "deny"
            }
        ]
    },
    {
        "role": "putAllowedEverywhere",
        "permissions": [
            {
                "resource": "*",
                "methods": [
                    "PUT"
                ],
                "action": "allow"
            }
        ]
    },
    {
        "role": "allMethodsAllowedOnUser",
        "permissions": [
            {
                "resource": "user",
                "methods": "*",
                "action": "allow"
            }
        ]
    }
];

const route = {
    name: 'putuser',
    method: 'PUT',
    path: '/user',
    spec: {
        method: 'PUT',
        path: '/user',
        version: '1.0.0'
    }
};

const config  = {
    aclFilename:   'acl.json',
    aclPath:       './defaults',
    baseApiUrl:    '/',
    errorMessage:  'You are not authorized to access this resource'
};

const roles   = ["user"];

// ACL Error Logging
authProcessor(config, null, roles, route, (error) => {

});

// Roles Error Logging
authProcessor(config, acl, null, route, (error) => {

});

// Route Error Logging
authProcessor(config, acl, roles, null, (error) => {

});

// No permissions for role
authProcessor(config, acl, ['no'], route, (error) => {

});

// Success
authProcessor(config, acl, roles, route, (error) => {

});