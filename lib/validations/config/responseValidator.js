// Node/NPM Modules

// Local variables
const utilities = require('../../utilities/utilities');

// Module functions
module.exports = function (response) {

    // If null was passed in by user, it can be ignored
    // since response is not required.
    if(!response) return response;

    if(!utilities.isObjLiteral(response))       throw new Error(`${utilities.moduleInfo.name}: Response is not an object literal`);
    if(!response.hasOwnProperty('status'))      throw new Error(`${utilities.moduleInfo.name}: Response does not contain the required status property`);
    if(!(typeof response.status  === "string")) throw new Error(`${utilities.moduleInfo.name}: Response status is not a String`);
    if(!response.hasOwnProperty('message'))     throw new Error(`${utilities.moduleInfo.name}: Response does not contain the required message property`);
    if(!(typeof response.message === "string")) throw new Error(`${utilities.moduleInfo.name}: Response message is not a String`);

    return response;
};