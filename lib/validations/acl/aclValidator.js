// Node/NPM Modules
const fs = require('fs');

// Local variables
const utilities = require('../../utilities/utilities');
const policyValidator = require('./aclPolicyValidator');

// Module functions
module.exports = function (aclFilePath) {

    let aclData;

    if(!fs.existsSync(aclFilePath)) throw new Error(`${utilities.moduleInfo.name}: ACL file could not be located - path: ${aclFilePath}.`);

    try{
        let rawData = fs.readFileSync(aclFilePath);
        aclData = JSON.parse(rawData);
    } catch(err){
        if(err instanceof SyntaxError){
            throw err;
        } else {
            throw new Error(`${utilities.moduleInfo.name}: Unable to open or process ACL file.`);
        }
    }

    if(!aclData instanceof Array)   throw new Error(`${utilities.moduleInfo.name}: ACL root is not an array.`);
    if(aclData.length === 0)        throw new Error(`${utilities.moduleInfo.name}: ACL does not contain at least one policy object.`);

    aclData.forEach((element) => {
        policyValidator(element);
    });

    return aclData;
};