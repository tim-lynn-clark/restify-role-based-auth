// Node/NPM Modules
const path  = require('path');

// Local Modules
const configValidator           = require('./validations/config/configValidator');
const aclValidator              = require('./validations/acl/aclValidator');
const userValidator             = require('./validations/auth/userValidator');
const routeValidator            = require('./validations/auth/routeValidator');
const authorizationProcessor    = require('./processors/authorizationProcessor');
const debug                     = require('./utilities/debuggers').component.indexLogger;


// Local Variables
let defaults  = {
    aclFilename:   'acl.json',
    aclPath:       './config',
    baseApiUrl:    '/',
    errorMessage:  'You are not authorized to access this resource'
};

// Module Functions
module.exports = function(config) {

    // Validate config parameters
    configValidator(config);

    // Merge user-supplied config into defaults, if provided
    if(config && typeof config === 'object'){
        Object.assign(defaults, config);
    }

    // Check for ACL file and validate
    let aclFilePath = path.resolve(defaults.aclPath);
    aclFilePath     = aclFilePath + '/' + defaults.aclFilename;
    let acl         = aclValidator(aclFilePath);

    debug(`INFO: config -> ${JSON.stringify(defaults)}`);
    debug(`INFO: ACL filepath -> ${JSON.stringify(aclFilePath)}`);

    // Middleware: authorization function
    let roleBasedAuth = function(req, res, next) {

        const route = routeValidator(req.route);
        const roles = userValidator(req.user);

        debug(`INFO: route provided ${JSON.stringify(route)}`);
        debug(`INFO: user provided ${JSON.stringify(req.user)}`);
        debug(`INFO: roles provided ${JSON.stringify(roles)}`);

        authorizationProcessor(defaults, acl, roles, route, next);
    };

    // FIXME: Instead of using Express-Unless to handle routes that are not
    // protected based on user-role(s), but users have to be logged in to access,
    // add this functionality to the ACL file and let this library handle it

    // Allow user to add specific express conditions that stop this middleware
    // from executing, such as specific URL paths.
    roleBasedAuth.unless = require('express-unless');

    return roleBasedAuth;
};


// Private functions
