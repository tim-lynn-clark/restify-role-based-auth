// Node/NPM Modules
const restErrors = require('restify-errors');
const debug      = require('../utilities/debuggers').component.authorizationProcessorLogger;

// Local variables
let _baseApiUrl, _errorMessage, _acl, _roles, _route, _next, _rolePolicies;

// Private functions
function buildError(){
    return new restErrors.UnauthorizedError(_errorMessage);
}

function parameterValidationError(){

    if(!_acl || !(_acl instanceof Array) || _acl.length === 0){
        debug('ERROR: ACL is non-existent, not an Array, or no entries in the Array');
        return true;
    }

    if(!_roles || !(_roles instanceof Array) || _roles.length === 0){
        debug('ERROR: Roles is non-existent, not an Array, or no entries in the Array');
        return true;
    }

    if(!_route || !(_route instanceof Object)){
        debug('ERROR: Route is non-existent or not an object');
        return true;
    }

    return false;
}

function locateRolePolicies(){
    _rolePolicies = {};

    for(let i=0; i<_roles.length; i++){
        let role = _roles[i];
        for(let j=0; j<_acl.length; j++){
            let policy = _acl[j];
            if(policy.role.toLowerCase() === role.toLowerCase()){
                // May be more than one policy per role
                if(_rolePolicies.hasOwnProperty(role)){
                    _rolePolicies[role].push(policy);
                } else {
                    _rolePolicies[role] = [policy];
                }
            }
        }
    }
}

function resourceLocated(permissionResource, routePath){

    let located = false;

    permissionResource = permissionResource.toLowerCase();
    routePath = routePath.toLowerCase();

    if(!(permissionResource === '*')){
        permissionResource = _baseApiUrl + permissionResource;
    }

    if(permissionResource.toLowerCase() === '*' || permissionResource === routePath){
        located = true;
    }

    debug(`INFO: resource:path -> ${permissionResource}:${routePath}`);
    debug(`INFO: resource located -> ${located}`);

    return located;
}

function methodMatch(permissionMethod, routeMethod){

    let match = false;

    permissionMethod = permissionMethod.toLowerCase();
    routeMethod = routeMethod.toLowerCase();

    if(permissionMethod === '*' || permissionMethod === routeMethod){
        match = true;
    }

    debug(`INFO: permissionMethod:routeMethod -> ${permissionMethod}:${routeMethod}`);
    debug(`INFO: method located -> ${match}`);

    return match;
}

function methodLocated(permissionMethod, routeMethod){

    let located = false;

    // permission.method can be a string wildcard '*' or an array of HTTP Verbs as strings
    if(typeof permissionMethod === "string"){
        located = methodMatch(permissionMethod, routeMethod);
    } else if((permissionMethod instanceof Array)){
        for(let l=0; l<permissionMethod.length; l++){
            let method = permissionMethod[l];
           located =  methodMatch(method, routeMethod);
           if(located) break;
        }
    }

    return located;
}

// Module functions
module.exports = function (config, acl, roles, route, next) {

    _baseApiUrl     = config.baseApiUrl;
    _errorMessage   = config.errorMessage;
    _acl            = acl;
    _roles          = roles;
    _route          = route;
    _next           = next;

    if(parameterValidationError()) return next(buildError());

    locateRolePolicies();

    let authorized          = false;
    let rolesWithPolicies   = Object.getOwnPropertyNames(_rolePolicies);

    // No policy for roles provided
    if(rolesWithPolicies.length === 0){
        debug(`INFO: No policies found for roles provided -> ${JSON.stringify(_roles)}`);
        debug(`INFO: ACL provided -> ${JSON.stringify(_acl)}`);
        return next(buildError());
    }

    // Policy & permission checks
    for(let i=0; i<rolesWithPolicies.length; i++){
        let role = rolesWithPolicies[i];
        let policies = _rolePolicies[role];

        for(let j=0; j<policies.length; j++){
            let policy = policies[j];

            for(let k=0; k<policy.permissions.length; k++){
                let permission = policy.permissions[k];

                if(resourceLocated(permission.resource, route.path)){
                    if(methodLocated(permission.methods, route.method)){
                        authorized = true;
                    }
                }

                debug(`INFO: Action -> ${permission.action}`);
                if(permission.action === "deny"){
                    authorized = false;
                }

                if(authorized) break;
            }
            if(authorized) break;
        }
        if(authorized) break;
    }

    debug(`INFO: Authorized -> ${authorized}`);

    if(authorized){
        return next();
    } else {
        return next(buildError());
    }
};