const pkg = require('../../package');

module.exports.moduleInfo = {
    name: pkg.name,
    version: pkg.version
};

module.exports.isObjLiteral = function isObjLiteral(_obj) {
    let _test  = _obj;
    return (  typeof _obj !== 'object' || _obj === null ?
            false :
            (
                (function () {
                    while (true) {
                        if (  Object.getPrototypeOf( _test = Object.getPrototypeOf(_test)  ) === null) {
                            break;
                        }
                    }
                    return Object.getPrototypeOf(_obj) === _test;
                })()
            )
    );
};
